package com.xa.project313_api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.xa.project313_api.models.Resep;
import com.xa.project313_api.repositories.ResepRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class ResepController {

    @Autowired
    ResepRepository resepRepository;

    @GetMapping("/resep")
    public ResponseEntity<List<Resep>> getAll(@RequestParam(required = false) Long id) {
        try {
            List<Resep> resep = null;
            if (id != null) {
                resep = this.resepRepository.getResepByAppointmentId(id);
            } else {
                resep = this.resepRepository.findAll();
            }
            return new ResponseEntity<List<Resep>>(resep, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Resep>>(HttpStatus.NO_CONTENT);
        }
    }
}
