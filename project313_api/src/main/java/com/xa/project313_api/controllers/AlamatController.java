package com.xa.project313_api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.xa.project313_api.models.Alamat;
import com.xa.project313_api.repositories.AlamatRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class AlamatController {

    @Autowired
    AlamatRepository alamatRepository;

    @GetMapping("/alamat")
    public ResponseEntity<List<Alamat>> getAll() {
        try {
            List<Alamat> resep = this.alamatRepository.findAll();
            return new ResponseEntity<List<Alamat>>(resep, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<Alamat>>(HttpStatus.NO_CONTENT);
        }
    }
}
