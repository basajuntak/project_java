package com.xa.project313_api.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.xa.project313_api.models.JenjangPendidikan;
import com.xa.project313_api.repositories.JenjangPendidikanRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class JenjangPendidikanController {
    @Autowired
    JenjangPendidikanRepository jenjangPendidikanRepository;

    @GetMapping("/education_level")
    public ResponseEntity<List<JenjangPendidikan>> getAll() {
        try {
            List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository.findAll();
            return new ResponseEntity<List<JenjangPendidikan>>(jenjangPendidikan, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<JenjangPendidikan>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/education_level/{id}")
    public ResponseEntity<?> getJenjangPendidikanById(@PathVariable("id") Long id) {
        try {
            JenjangPendidikan jenjangPendidikan = this.jenjangPendidikanRepository.findById(id).orElse(null);
            if (jenjangPendidikan != null) {
                return new ResponseEntity<JenjangPendidikan>(jenjangPendidikan, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("JenjangPendidikan Not Found!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<JenjangPendidikan>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/education_level")
    public ResponseEntity<List<JenjangPendidikan>> insertJenjangPendidikan(
            @RequestBody List<JenjangPendidikan> jenjangPendidikan) {
        try {
            if (jenjangPendidikan.size() > 1) {
                this.jenjangPendidikanRepository.saveAll(jenjangPendidikan);
            } else {
                this.jenjangPendidikanRepository.save(jenjangPendidikan.get(0));
            }

            return new ResponseEntity<List<JenjangPendidikan>>(jenjangPendidikan, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<JenjangPendidikan>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/education_level/search")
    public ResponseEntity<List<JenjangPendidikan>> getJenjangPendidikanByName(
            @RequestParam(required = false) String name) {
        try {
            List<JenjangPendidikan> jenjangPendidikan = this.jenjangPendidikanRepository
                    .getJenjangPendidikanByName(name);
            return new ResponseEntity<List<JenjangPendidikan>>(jenjangPendidikan, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<JenjangPendidikan>>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/education_level/{id}")
    public ResponseEntity<?> deleteJenjangPendidikan(@PathVariable("id") Long id, HttpSession sess) {
        try {
            JenjangPendidikan jenjangPendidikan = this.jenjangPendidikanRepository.findById(id).orElse(null);
            if (jenjangPendidikan != null) {
                this.jenjangPendidikanRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("golongan darah deleted!");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Deleted failed");
            }

        } catch (Exception e) {
            return new ResponseEntity<JenjangPendidikan>(HttpStatus.NO_CONTENT);

        }
    }

    @PutMapping("/education_level/{id}")
    public ResponseEntity<JenjangPendidikan> edititemCategory(
            @RequestBody JenjangPendidikan jenjangPendidikan,
            @PathVariable("id") Long id, HttpSession sess) {
        try {
            JenjangPendidikan _jenjangPendidikan = this.jenjangPendidikanRepository.findById(id).orElse(null);
            if (_jenjangPendidikan != null) {
                this.jenjangPendidikanRepository.save(jenjangPendidikan);
                return new ResponseEntity<JenjangPendidikan>(jenjangPendidikan, HttpStatus.OK);
            } else {
                return new ResponseEntity<JenjangPendidikan>(HttpStatus.NO_CONTENT);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<JenjangPendidikan>(HttpStatus.NO_CONTENT);

        }

    }
}
