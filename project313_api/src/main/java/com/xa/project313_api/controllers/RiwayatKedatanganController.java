package com.xa.project313_api.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.xa.project313_api.models.BuatJanji;
import com.xa.project313_api.repositories.RiwayatKedatanganRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class RiwayatKedatanganController {

    @Autowired
    RiwayatKedatanganRepository riwayatKedatanganRepository;

    @GetMapping("/riwayat")
    public ResponseEntity<List<BuatJanji>> getAll() {
        try {
            List<BuatJanji> resep = this.riwayatKedatanganRepository.findAll();
            return new ResponseEntity<List<BuatJanji>>(resep, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<BuatJanji>>(HttpStatus.NO_CONTENT);
        }
    }
}
