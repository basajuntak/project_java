package com.xa.project313_api.controllers;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.xa.project313_api.models.GolonganDarah;
import com.xa.project313_api.repositories.GolonganDarahRepository;

@RestController
@RequestMapping("/api")
@CrossOrigin("*")
public class GolonganDarahController {

    @Autowired
    GolonganDarahRepository golonganDarahRepository;

    @GetMapping("/blood_group")
    public ResponseEntity<List<GolonganDarah>> getAll() {
        try {
            List<GolonganDarah> golonganDarah = this.golonganDarahRepository.findAll();
            return new ResponseEntity<List<GolonganDarah>>(golonganDarah, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<GolonganDarah>>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/blood_group/{id}")
    public ResponseEntity<?> getGolonganDarahById(@PathVariable("id") Long id) {
        try {
            GolonganDarah golonganDarah = this.golonganDarahRepository.findById(id).orElse(null);
            if (golonganDarah != null) {
                return new ResponseEntity<GolonganDarah>(golonganDarah, HttpStatus.OK);
            } else {
                return ResponseEntity.status(HttpStatus.NOT_FOUND).body("GolonganDarah Not Found!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<GolonganDarah>(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping("/blood_group")
    public ResponseEntity<GolonganDarah> insertGolonganDarah(@RequestBody GolonganDarah golonganDarah) {
        try {
            this.golonganDarahRepository.save(golonganDarah);
            return new ResponseEntity<GolonganDarah>(golonganDarah, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<GolonganDarah>(HttpStatus.NO_CONTENT);
        }
    }

    @GetMapping("/blood_group/search")
    public ResponseEntity<List<GolonganDarah>> getGolonganDarahByName(
            @RequestParam(required = false) String descrtiption) {
        try {
            List<GolonganDarah> golonganDarah = this.golonganDarahRepository
                    .getGolonganDarahByDescrtiption(descrtiption);
            return new ResponseEntity<List<GolonganDarah>>(golonganDarah, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<List<GolonganDarah>>(HttpStatus.NO_CONTENT);
        }
    }

    @DeleteMapping("/blood_group/{id}")
    public ResponseEntity<?> deleteGolonganDarah(@PathVariable("id") Long id, HttpSession sess) {
        try {
            GolonganDarah golonganDarah = this.golonganDarahRepository.findById(id).orElse(null);
            if (golonganDarah != null) {
                this.golonganDarahRepository.deleteById(id);
                return ResponseEntity.status(HttpStatus.OK).body("golongan darah deleted!");
            } else {
                return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Deleted failed");
            }

        } catch (Exception e) {
            return new ResponseEntity<GolonganDarah>(HttpStatus.NO_CONTENT);

        }
    }

    @PutMapping("/blood_group/{id}")
    public ResponseEntity<GolonganDarah> edititemCategory(
            @RequestBody GolonganDarah golonganDarah,
            @PathVariable("id") Long id, HttpSession sess) {
        try {
            GolonganDarah _golonganDarah = this.golonganDarahRepository.findById(id).orElse(null);
            if (_golonganDarah != null) {
                this.golonganDarahRepository.save(golonganDarah);
                return new ResponseEntity<GolonganDarah>(golonganDarah, HttpStatus.OK);
            } else {
                return new ResponseEntity<GolonganDarah>(HttpStatus.NO_CONTENT);
            }

        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<GolonganDarah>(HttpStatus.NO_CONTENT);

        }

    }

}
