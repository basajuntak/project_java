package com.xa.project313_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.project313_api.models.GolonganDarah;

@Repository
public interface GolonganDarahRepository extends JpaRepository<GolonganDarah, Long> {

    @Query(value = "SELECT g FROM GolonganDarah g WHERE g.descrtiption LIKE %?1%")
    public List<GolonganDarah> getGolonganDarahByDescrtiption(String descrtiption);
}
