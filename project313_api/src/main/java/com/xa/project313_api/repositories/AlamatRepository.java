package com.xa.project313_api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.xa.project313_api.models.Alamat;

@Repository
public interface AlamatRepository extends JpaRepository<Alamat, Long> {

}
