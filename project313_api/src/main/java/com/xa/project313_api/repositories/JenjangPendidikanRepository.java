package com.xa.project313_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.project313_api.models.JenjangPendidikan;

@Repository
public interface JenjangPendidikanRepository extends JpaRepository<JenjangPendidikan, Long> {

    @Query(value = "SELECT j FROM JenjangPendidikan j WHERE j.name LIKE %?1%")
    public List<JenjangPendidikan> getJenjangPendidikanByName(String name);
}
