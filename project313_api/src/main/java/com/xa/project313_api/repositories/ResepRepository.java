package com.xa.project313_api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.xa.project313_api.models.Resep;

@Repository
public interface ResepRepository extends JpaRepository<Resep, Long> {

    @Query(value = "SELECT r FROM Resep r WHERE r.appointment_id.id = ?1")
    public List<Resep> getResepByAppointmentId(Long appointment_id);
}
