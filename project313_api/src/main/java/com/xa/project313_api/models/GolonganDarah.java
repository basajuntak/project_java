package com.xa.project313_api.models;

import javax.persistence.*;

@Entity
@Table(name = "m_blood_group")
public class GolonganDarah {
    @Id // menentukan primary keynya
    @GeneratedValue(strategy = GenerationType.IDENTITY) // membuat kolom ini(id) menjadi auto increment
    @Column(name = "id") // membuat kolom
    private Long Id; // guna tiap variabel ini

    @Column(name = "kode", length = 5)
    private String code;

    @Column(name = "deskripsi", length = 255)
    private String descrtiption;

    public GolonganDarah() {
    }

    public GolonganDarah(Long id, String code, String descrtiption) {
        Id = id;
        this.code = code;
        this.descrtiption = descrtiption;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescrtiption() {
        return descrtiption;
    }

    public void setDescrtiption(String descrtiption) {
        this.descrtiption = descrtiption;
    }

}
