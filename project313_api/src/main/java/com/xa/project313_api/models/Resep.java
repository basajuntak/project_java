package com.xa.project313_api.models;

import javax.persistence.*;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "t_prescription")
public class Resep {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "appointment_id", insertable = false, updatable = false)
    @JsonIgnore
    public BuatJanji appointment_id;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "dosage", length = 255)
    private String dosage;

    @Column(name = "directions", length = 255)
    private String directions;

    @Column(name = "time", length = 100)
    private String time;

    @Column(name = "notes", length = 255)
    private String notes;

    public Resep() {
    }

    public Resep(Long id, BuatJanji appointment_id, String dosage, String directions, String time, String notes) {
        Id = id;
        this.appointment_id = appointment_id;
        this.dosage = dosage;
        this.directions = directions;
        this.time = time;
        this.notes = notes;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public BuatJanji getAppointment_id() {
        return appointment_id;
    }

    public void setAppointment_id(BuatJanji appointment_id) {
        this.appointment_id = appointment_id;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getDirections() {
        return directions;
    }

    public void setDirections(String directions) {
        this.directions = directions;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

}
