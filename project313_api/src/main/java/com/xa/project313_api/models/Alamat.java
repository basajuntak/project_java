package com.xa.project313_api.models;

import javax.persistence.*;

@Entity
@Table(name = "m_biodata_address")
public class Alamat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata_id;

    @Column(name = "label", length = 100)
    private String label;

    @Column(name = "recipient", length = 100)
    private String recipient;

    @Column(name = "recipient_phone_number", length = 15)
    private String recipientPhoneNumber;

    @ManyToOne
    @JoinColumn(name = "location_id", insertable = false, updatable = false)
    public Lokasi location_id;

    @Column(name = "postal_code", length = 10)
    private String postalCode;

    @Column(name = "address", length = 255)
    private String address;

    public Alamat() {
    }

    public Alamat(Long id, Biodata biodata_id, String label, String recipient, String recipientPhoneNumber,
            Lokasi location_id, String postalCode, String address) {
        Id = id;
        this.biodata_id = biodata_id;
        this.label = label;
        this.recipient = recipient;
        this.recipientPhoneNumber = recipientPhoneNumber;
        this.location_id = location_id;
        this.postalCode = postalCode;
        this.address = address;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Biodata getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Biodata biodata_id) {
        this.biodata_id = biodata_id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public String getRecipientPhoneNumber() {
        return recipientPhoneNumber;
    }

    public void setRecipientPhoneNumber(String recipientPhoneNumber) {
        this.recipientPhoneNumber = recipientPhoneNumber;
    }

    public Lokasi getLocation_id() {
        return location_id;
    }

    public void setLocation_id(Lokasi location_id) {
        this.location_id = location_id;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
