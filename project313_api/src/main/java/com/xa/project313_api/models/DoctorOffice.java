package com.xa.project313_api.models;

import javax.persistence.*;

@Entity
@Table(name = "t_doctor_office")
public class DoctorOffice {

    public DoctorOffice() {
    }

    public DoctorOffice(Long id, Doctor doctor_id, Long medical_facility_id, String specialization) {
        Id = id;
        this.doctor_id = doctor_id;
        this.medical_facility_id = medical_facility_id;
        this.specialization = specialization;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "doctor_id", insertable = false, updatable = false)
    public Doctor doctor_id;

    @Column(name = "medical_facility_id")
    public Long medical_facility_id;

    @Column(name = "specialization", length = 100)
    private String specialization;

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Doctor getDoctor_id() {
        return doctor_id;
    }

    public void setDoctor_id(Doctor doctor_id) {
        this.doctor_id = doctor_id;
    }

    public Long getMedical_facility_id() {
        return medical_facility_id;
    }

    public void setMedical_facility_id(Long medical_facility_id) {
        this.medical_facility_id = medical_facility_id;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

}
