package com.xa.project313_api.models;

import java.sql.Timestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "t_appointment")
public class BuatJanji {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "customer_id", insertable = false, updatable = false)
    public Customer customer_id;

    @ManyToOne
    @JoinColumn(name = "doctor_office_id", insertable = false, updatable = false)
    public DoctorOffice doctor_office_id;

    @Column(name = "doctor_office_schedule_id")
    private Long doctor_office_schedule_id;

    @Column(name = "doctor_office_treatment_id")
    private Long doctor_office_treatment_id;

    @CreatedDate
    @NotNull
    private Timestamp appointment_date;

    public BuatJanji() {
    }

    public BuatJanji(Long id, Customer customer_id, DoctorOffice doctor_office_id, Long doctor_office_schedule_id,
            Long doctor_office_treatment_id, @NotNull Timestamp appointment_date) {
        Id = id;
        this.customer_id = customer_id;
        this.doctor_office_id = doctor_office_id;
        this.doctor_office_schedule_id = doctor_office_schedule_id;
        this.doctor_office_treatment_id = doctor_office_treatment_id;
        this.appointment_date = appointment_date;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Customer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Customer customer_id) {
        this.customer_id = customer_id;
    }

    public DoctorOffice getDoctor_office_id() {
        return doctor_office_id;
    }

    public void setDoctor_office_id(DoctorOffice doctor_office_id) {
        this.doctor_office_id = doctor_office_id;
    }

    public Long getDoctor_office_schedule_id() {
        return doctor_office_schedule_id;
    }

    public void setDoctor_office_schedule_id(Long doctor_office_schedule_id) {
        this.doctor_office_schedule_id = doctor_office_schedule_id;
    }

    public Long getDoctor_office_treatment_id() {
        return doctor_office_treatment_id;
    }

    public void setDoctor_office_treatment_id(Long doctor_office_treatment_id) {
        this.doctor_office_treatment_id = doctor_office_treatment_id;
    }

    public Timestamp getAppointment_date() {
        return appointment_date;
    }

    public void setAppointment_date(Timestamp appointment_date) {
        this.appointment_date = appointment_date;
    }

}
