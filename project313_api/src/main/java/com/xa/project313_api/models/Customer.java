package com.xa.project313_api.models;

import java.sql.Timestamp;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.springframework.data.annotation.CreatedDate;

@Entity
@Table(name = "m_customer")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata_id;

    @Column(name = "gender", length = 1)
    private String gender;

    @CreatedDate
    @NotNull
    private Timestamp dob;

    @ManyToOne
    @JoinColumn(name = "blood_group_id", insertable = false, updatable = false)
    public GolonganDarah blood_group_id;

    @Column(name = "rhesus_type", length = 5)
    private String rhesusType;

    @Column(name = "height")
    private Double height;

    @Column(name = "weight")
    private Double weight;

    public Customer() {
    }

    public Customer(Long id, Biodata biodata_id, String gender, @NotNull Timestamp dob, GolonganDarah blood_group_id,
            String rhesusType, Double height, Double weight) {
        Id = id;
        this.biodata_id = biodata_id;
        this.gender = gender;
        this.dob = dob;
        this.blood_group_id = blood_group_id;
        this.rhesusType = rhesusType;
        this.height = height;
        this.weight = weight;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Biodata getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Biodata biodata_id) {
        this.biodata_id = biodata_id;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Timestamp getDob() {
        return dob;
    }

    public void setDob(Timestamp dob) {
        this.dob = dob;
    }

    public GolonganDarah getBlood_group_id() {
        return blood_group_id;
    }

    public void setBlood_group_id(GolonganDarah blood_group_id) {
        this.blood_group_id = blood_group_id;
    }

    public String getRhesusType() {
        return rhesusType;
    }

    public void setRhesusType(String rhesusType) {
        this.rhesusType = rhesusType;
    }

    public Double getHeight() {
        return height;
    }

    public void setHeight(Double height) {
        this.height = height;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
