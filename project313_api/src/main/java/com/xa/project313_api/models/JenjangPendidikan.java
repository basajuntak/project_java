package com.xa.project313_api.models;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "m_education_level")
public class JenjangPendidikan {

    @Id // menentukan primary keynya
    @GeneratedValue(strategy = GenerationType.IDENTITY) // membuat kolom ini(id) menjadi auto increment
    @Column(name = "id") // membuat kolom
    private Long Id; // guna tiap variabel ini

    @Column(name = "name", length = 255)
    @NotNull
    @NotBlank
    private String name;

    public JenjangPendidikan() {
    }

    public JenjangPendidikan(Long id, String name) {
        Id = id;
        this.name = name;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
