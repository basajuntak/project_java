package com.xa.project313_api.models;

import javax.persistence.*;

@Entity
@Table(name = "m_doctor")
public class Doctor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @ManyToOne
    @JoinColumn(name = "biodata_id", insertable = false, updatable = false)
    public Biodata biodata_id;

    @Column(name = "str", length = 50)
    private String str;

    public Doctor() {
    }

    public Doctor(Long id, Biodata biodata_id, String str) {
        Id = id;
        this.biodata_id = biodata_id;
        this.str = str;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public Biodata getBiodata_id() {
        return biodata_id;
    }

    public void setBiodata_id(Biodata biodata_id) {
        this.biodata_id = biodata_id;
    }

    public String getStr() {
        return str;
    }

    public void setStr(String str) {
        this.str = str;
    }
}
