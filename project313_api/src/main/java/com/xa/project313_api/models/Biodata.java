package com.xa.project313_api.models;

import javax.persistence.*;

@Entity
@Table(name = "m_biodata")
public class Biodata {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long Id;

    @Column(name = "fullname", length = 255)
    private String fullName;

    @Column(name = "mobile_phone", length = 15)
    private String mobilePhone;

    @Column(name = "image_path", length = 255)
    private String imagePath;

    public Biodata() {
    }

    public Biodata(Long id, String fullName, String mobilePhone, String imagePath) {
        Id = id;
        this.fullName = fullName;
        this.mobilePhone = mobilePhone;
        this.imagePath = imagePath;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long id) {
        Id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMobilePhone() {
        return mobilePhone;
    }

    public void setMobilePhone(String mobilePhone) {
        this.mobilePhone = mobilePhone;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
}
