package com.xa.project313.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/frontend")
public class GolonganDarahController {

    @GetMapping(value = "blood_group")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/blood_group/index");
        return view;
    }

}
