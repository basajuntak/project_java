package com.xa.project313.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/frontend")
public class JenjangPendidikanController {

    @GetMapping(value = "education_level")
    public ModelAndView index() {
        ModelAndView view = new ModelAndView("/education_level/index");
        return view;
    }
}
