package com.xa.project313;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Project313Application {

	public static void main(String[] args) {
		SpringApplication.run(Project313Application.class, args);
	}

}
